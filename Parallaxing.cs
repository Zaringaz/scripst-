﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallaxing : MonoBehaviour // Параллакс Изменение объекта относительно фона и от положение наблюдателя
{
    public Transform[] backGround; // Массив положения объектов которые будут использоваться в  Параллакс
    private float[] parallaxScale; // Массив пропорций паралакста для каждого обекта.
    public float smoothing = 1.0f; // Сглаживание Паралакса

    private Transform mainCam; //положение камеры
    private Vector3 previousCamPos; //Хранение позиции камеры в предыдущем кадре


    private void Awake()
    {
        mainCam = Camera.main.transform; //камера ставится на начальную точку
    }

    private void Start()
    {
        previousCamPos = mainCam.position; // предыдущий кадр имел позицию камеры
        parallaxScale = new float[backGround.Length]; //Создали новый массив длиной, количеством объектов использованых в параллаксе

        for (int i = 0; i < backGround.Length; i++)
        {
            parallaxScale[i] = backGround[i].position.z; // Записывает переврнутое знаечение по оси z каждого объекта 
        }
    }

    private void Update()
    {
        for (int i = 0; i < backGround.Length; i++)
        {
            float parallax = (previousCamPos.x - mainCam.position.x) * parallaxScale[i];//Где камера была потом куда сдвинулась помножить на пропорцию параллакса

            float backGroundTargetPosX = backGround[i].position.x + parallax;// На сколько будем перемещать наш задник по х

            //Делаем вектор 3 куда должен он переместиться
            Vector3 backGroundTargetPos = new Vector3(backGroundTargetPosX, backGround[i].position.y, backGround[i].position.z);

            // перемещаем с помощью Lerp и плавность регулируем 
            backGround[i].position = Vector3.Lerp(backGround[i].position, backGroundTargetPos, smoothing * Time.deltaTime);
        }
        previousCamPos = mainCam.position; //задаем новые координты от который будет идти эффект параллакса
    }
}
