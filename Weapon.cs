﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Weapon : MonoBehaviour
{
    
    private Animator animator;
    public int bullet = 10;
    public int damage = 10;

    public static Action onWeaponShoot;

    public string soundName;
  
    public virtual void OnPlayerShoot()
    {

    }

   void Start()
    {

        animator = GetComponent<Animator>();

        if (UIController.onShoot != null)
        {
            UIController.onShoot(bullet);
        }
    }

    private void OnEnable()
    {

        if (UIController.onShoot != null)
        {
            UIController.onShoot(bullet);
        }
        UIController.OnWeaponShoot += OnPlayerShoot;
    }
    private void OnDisable()
    {
        UIController.OnWeaponShoot -= OnPlayerShoot;
    }

    public void OnStartShoot()
    {
        animator.SetTrigger("Shoot");
        SoundPlayer.instance.PlaySound(soundName, 1);
        bullet--;

        if (UIController.onShoot != null)
        {
            UIController.onShoot(bullet);
        }

        if (onWeaponShoot != null)
        {
            onWeaponShoot();
        }
    }
}
