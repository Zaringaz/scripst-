﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SoundPlayer : MonoBehaviour
{
    public static SoundPlayer instance;

    public List<AudioClip> audioClip = new List<AudioClip>();

    void Awake()
    {

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);

            Object[] audioClipObjects = Resources.LoadAll("Sounds/Game");

            for (int i = 0; i < audioClipObjects.Length; i++)
            {
                audioClip.Add((AudioClip)(audioClipObjects[i]));
            }

        }
        else
        {
            Destroy(gameObject);
        }



    }

    public void PlaySound(string nameSound, float volume)
    {
        GameObject newSoundObject = new GameObject(nameSound);
        AudioSource audioSource = newSoundObject.AddComponent<AudioSource>();
        AudioClip clip;

        clip = audioClip.First(testClip => testClip.name == nameSound);

        audioSource.clip = clip;
        audioSource.volume = volume * CustomSettings.volume;
        audioSource.Play();

        Destroy(newSoundObject, clip.length);

        //for (int i = 0; i < audioClip.Count; i++)
        //{
        //    if (nameSound == audioClip[i].name)
        //    {
        //        clip = audioClip[i];
        //        break;
        //    }
        //}

    }

    void Update()
    {

    }

}
